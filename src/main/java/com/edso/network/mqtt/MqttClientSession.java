package com.edso.network.mqtt;

import lombok.Data;
import org.eclipse.paho.client.mqttv3.*;

@Data
public class MqttClientSession implements MqttCallbackExtended {

    private final String id;
    private final String host;
    private final int port;
    private final String topic;
    private final int qos;

    private MqttClient client;

    public MqttClientSession(String id, String host, int port, String topic, int qos) {
        this.id = id;
        this.host = host;
        this.port = port;
        this.topic = topic;
        this.qos = qos;
    }

    public void connect(){
        try {
            String serverUri = "tcp://" + host + ":" + port;
            client = new MqttClient(serverUri, id);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            options.setAutomaticReconnect(true);
            options.setConnectionTimeout(60);
            client.setCallback(this);
            client.connect(options);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
        System.out.println("Connection lost, trying to reconnect");
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) {
        System.out.println("topic - " + s + ": " + mqttMessage);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    @Override
    public void connectComplete(boolean b, String s) {
        try{
            client.subscribe(topic, 1);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
