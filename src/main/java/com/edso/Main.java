package com.edso;

import com.edso.network.mqtt.MqttClientSession;

public class Main {
    public static void main(String[] args) {
        MqttClientSession mqttClientSession = new MqttClientSession("client",
                "192.168.205.128",
                10004,
                "topic/test",
                1);
        mqttClientSession.connect();
    }
}
